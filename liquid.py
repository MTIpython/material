r"""
.. module:: MTIpython.material.liquid
    :platform: Unix, Windows
    :synopsis: Liquid material types.

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""

from MTIpython.core.binder import Binder, Function, Valid
from MTIpython.fluid.principal.core import Re_bingham, Re_newton
from MTIpython.material.base import Category, Matter
from MTIpython.material.principal.core import dynamic_viscosity, kinematic_viscosity
from MTIpython.mtimath.types import inf
from MTIpython.units import u

__all__ = ['Liquid', 'Bingham']


class Liquid(Matter):

    def __init__(self, p=None, mu=None, nu=None, Pr=None, **kwargs):
        r"""
        A liquid is a nearly incompressible fluid that conforms to the shape of its container but retains a (nearly)
        constant volume independent of pressure. As such, it is one of the four fundamental states of matter (the
        others being solid, gas, and plasma), and is the only state with a definite volume but no fixed shape. A
        liquid is made up of tiny vibrating particles of matter, such as atoms, held together by intermolecular
        bonds. Water is, by far, the most common liquid on Earth. Like a gas, a liquid is able to flow and take the
        shape of a container. Most liquids resist compression, although others can be compressed. Unlike a gas,
        a liquid does not disperse to fill every space of a container, and maintains a fairly constant density. A
        distinctive property of the liquid state is surface tension, leading to wetting phenomena. The density of a
        liquid is usually close to that of a solid, and much higher than in a gas. Therefore, liquid and solid are
        both termed condensed matter. On the other hand, as liquids and gases share the ability to flow,
        they are both called fluids. Although liquid water is abundant on Earth, this state of matter is actually the
        least common in the known universe, because liquids require a relatively narrow temperature/pressure range to
        exist. Most known matter in the universe is in gaseous form (with traces of detectable solid matter) as
        interstellar clouds or in plasma form within stars.

        source: `Wikipedia <https://en.wikipedia.org/wiki/Liquid>`_

        Args:
            name (str): he common name for the material i.e. S235JR or Water
            rho (:class:`~pint:.Quantity` or :class:`.Binder`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
            T (:class:`~pint:.Quantity` or :class:`.Binder`): Temperature :math:`T` in :math:`[T]`
            c_p (:class:`~pint:.Quantity` or :class:`.Binder`): Specific heat at constant pressure :math:`c_p` in
             :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`~pint:.Quantity` or :class:`.Binder`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`~pint:.Quantity` or :class:`.Binder`): Mass :math:`m` in :math:`[M^{1}]`
            V (:class:`~pint:.Quantity` or :class:`.Binder`): Volume :math:`V` in :math:`[L^{3}]`
            v (:class:`~pint:.Quantity` or :class:`.Binder`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
            gamma (:class:`~pint:.Quantity` or :class:`.Binder`): Specific weight :math:`\gamma` in
             :math:`[M^{1} L^{-2} t^{-2}]`
            p (:class:`~pint:.Quantity` or :class:`.Binder`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
            mu (:class:`~pint:.Quantity` or :class:`.Binder`): Dynamic viscosity :math:`\mu` in
             :math:`[M^{1} L^{-1} t^{-1}]`
            nu (:class:`~pint:.Quantity` or :class:`.Binder`): Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`
            Pr (:class:`~pint:.Quantity` or :class:`.Binder`): Prandtl number :math:`Pr` in :math:`[-]`
            CAS: Chemical Abstracts Service number
            **kwargs: User set attributes

        Attributes:
            name (str): he common name for the material i.e. S235JR or Water
            sn (str): str: Short name for the material. When the :attr:`.name` consists of multiple  words, the short
             name is build from all first letters. When the name consist of a single word, the first two letters are
             used
            rho (:class:`.Valid`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`.
             rho is bound with (:attr:`.v`) or (:attr:`.gamma`) or (:attr:`.V` and :attr:`.m`) using :func:`.density`
             function
            T (:class:`.Valid`): Temperature :math:`T` in :math:`[T]`
            category (:class:`~Category`): The Material category
            c_p (:class:`.Valid`): Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`.Valid`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`.Valid`): Mass :math:`m` in :math:`[M^{1}]`
             m is bound with (:attr:`.rho` and :attr:`.V`) or (:attr:`.gamma` and :attr:`.v`) or (:attr:`.v` and
             :attr:`.V`) using :func:`.mass` function
            V (:class:`.Valid`): Volume :math:`V` in :math:`[L^{3}]`.
             V is bound with (:attr:`.rho` and :attr:`.m`) or (:attr:`.gamma` and :attr:`.m`) or (:attr:`.v` and
             :attr:`.m`) using :func:`.volume` function
            v (:class:`.Valid`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`.
             v is bound with (:attr:`.rho`) or (:attr:`.m` and :attr:`.V`) using :func:`.specific_volume` function
            k (:class:`.Valid`): Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`.Valid`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`.
             gamma is bound with (:attr:`.rho`) or (:attr:`.v`) or (:attr:`.m` and :attr:`.V`) using
             :func:`.specific_weight` function
            p (:class:`.Valid`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
            mu (:class:`.Valid`): Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`.
             mu is bound with (:attr:`.rho`, :attr:`.nu`) using :func:`.dynamic_viscosity`
            nu (:class:`.Valid`): Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`.
             nu is bound with (:attr:`.rho`, :attr:`.mu`) using :func:`.kinematic_viscosity`
            Pr (:class:`.Valid`): Prandtl number :math:`Pr` in :math:`[-]`
            CAS: Chemical Abstracts Service number

        Note:
            Each creation triggers an info log entry, specifying the material specifications
        """
        self.p = Binder(value=p, bounds=(0., inf), unit=u.Pa)
        self.nu = Binder(value=nu, bounds=(inf, -inf), unit=u.m ** 2 / u.s,
                         func1=Function(kinematic_viscosity, rho='rho', mu='mu'))
        self.mu = Binder(value=mu, bounds=(inf, -inf), unit=u.Pa * u.s,
                         func1=Function(dynamic_viscosity, rho='rho', nu='nu'))
        self.Pr = Binder(value=Pr, bounds=(-inf, inf), unit=u.dimensionless)
        super(Liquid, self).__init__(category=kwargs.pop('category', Category.FLUID), **kwargs)

    p = Valid()
    r""":class:`.Valid`: Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`"""

    nu = Valid()
    r""":class:`.Valid`: Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`"""

    mu = Valid()
    r""":class:`.Valid`: Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`"""

    Pr = Valid()
    r""":class:`.Valid`: Prandtl number :math:`Pr` in :math:`[-]`"""

    _mat = ['name', 'sn', 'category', 'T', 'rho', 'p', 'c_p', 'v', 'k', 'gamma', 'M', 'm', 'V', 'nu', 'mu', 'Pr']
    """list(str): the variables which describe the state of the material. Override this list for each child """

    _Re = staticmethod(Re_newton)
    r""" Calculate Reynolds with Re_newton"""


class Bingham(Liquid):

    def __init__(self, tau=None, eta=None, **kwargs):
        r"""
        Bingham fluids, This class describes the basic properties of a such as, dynamic or kinematic viscosity, specific
        heat under constant P and the yield stress :math:`\tau_{y}` A Bingham plastic is a viscoplastic material that
        behaves as a rigid body at low stresses but flows as a viscous fluid at high stress. It is named after Eugene C.
        Bingham who proposed its mathematical form. It is used as a common mathematical model of mud flow in drilling
        engineering, and in the handling of slurries. A common example is toothpaste, which will not be extruded until a
        certain pressure is applied to the tube. It then is pushed out as a solid plug.

        Source: `Wikipedia <https://en.wikipedia.org/wiki/Bingham_plastic>`_

        Args:
            name (str): he common name for the material i.e. S235JR or Water
            rho (:class:`~pint:.Quantity` or :class:`.Binder`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
            T (:class:`~pint:.Quantity` or :class:`.Binder`): Temperature :math:`T` in :math:`[T]`
            c_p (:class:`~pint:.Quantity` or :class:`.Binder`): Specific heat at constant pressure :math:`c_p` in
             :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`~pint:.Quantity` or :class:`.Binder`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`~pint:.Quantity` or :class:`.Binder`): Mass :math:`m` in :math:`[M^{1}]`
            V (:class:`~pint:.Quantity` or :class:`.Binder`): Volume :math:`V` in :math:`[L^{3}]`
            v (:class:`~pint:.Quantity` or :class:`.Binder`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
            gamma (:class:`~pint:.Quantity` or :class:`.Binder`): Specific weight :math:`\gamma` in
             :math:`[M^{1} L^{-2} t^{-2}]`
            k (:class:`~pint:.Quantity` or :class:`.Binder`): Thermal conductivity :math:`k` in
             :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            p (:class:`~pint:.Quantity` or :class:`.Binder`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
            mu (:class:`~pint:.Quantity` or :class:`.Binder`): Dynamic viscosity :math:`\mu` in
             :math:`[M^{1} L^{-1} t^{-1}]`
            nu (:class:`~pint:.Quantity` or :class:`.Binder`): Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`
            Pr (:class:`~pint:.Quantity` or :class:`.Binder`): Prandtl number :math:`Pr` in :math:`[-]`
            tau (:class:`~pint:.Quantity` or :class:`.Binder`): Yield stress :math:`\tau` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            eta (:class:`~pint:.Quantity` or :class:`.Binder`): Tangential viscosity :math:`\eta` in
             :math:`[M^{1} L^{-1} t^{-1}]`
            CAS: Chemical Abstracts Service number
            **kwargs: User set attributes

        Attributes:
            name (str): he common name for the material i.e. S235JR or Water
            sn (str): str: Short name for the material. When the :attr:`.name` consists of multiple  words, the short
             name is build from all first letters. When the name consist of a single word, the first two letters are
             used
            rho (:class:`.Valid`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`.
             rho is bound with (:attr:`.v`) or (:attr:`.gamma`) or (:attr:`.V` and :attr:`.m`) using :func:`.density`
             function
            T (:class:`.Valid`): Temperature :math:`T` in :math:`[T]`
            category (:class:`~Category`): The Material category
            c_p (:class:`.Valid`): Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`.Valid`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`.Valid`): Mass :math:`m` in :math:`[M^{1}]`
             m is bound with (:attr:`.rho` and :attr:`.V`) or (:attr:`.gamma` and :attr:`.v`) or (:attr:`.v` and
             :attr:`.V`) using :func:`.mass` function
            V (:class:`.Valid`): Volume :math:`V` in :math:`[L^{3}]`.
             V is bound with (:attr:`.rho` and :attr:`.m`) or (:attr:`.gamma` and :attr:`.m`) or (:attr:`.v` and
             :attr:`.m`) using :func:`.volume` function
            v (:class:`.Valid`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`.
             v is bound with (:attr:`.rho`) or (:attr:`.m` and :attr:`.V`) using :func:`.specific_volume` function
            k (:class:`.Valid`): Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`.Valid`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`.
             gamma is bound with (:attr:`.rho`) or (:attr:`.v`) or (:attr:`.m` and :attr:`.V`) using
             :func:`.specific_weight` function
            p (:class:`.Valid`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
            mu (:class:`.Valid`): Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`.
             mu is bound with (:attr:`.rho`, :attr:`.nu`) using :func:`.dynamic_viscosity`
            nu (:class:`.Valid`): Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`.
             nu is bound with (:attr:`.rho`, :attr:`.mu`) using :func:`.kinematic_viscosity`
            Pr (:class:`.Valid`): Prandtl number :math:`Pr` in :math:`[-]`
            tau (:class:`.Valid`): Yield stress :math:`\tau` in :math:`[M^{1} L^{-1} t^{-2}]`
            eta (:class:`.Valid`): Tangential viscosity :math:`\eta` in :math:`[M^{1} L^{-1} t^{-1}]`
            CAS: Chemical Abstracts Service number

        Note:
            Each creation triggers an info log entry, specifying the material specifications
        """
        self.tau = Binder(value=tau, bounds=(inf, -inf), unit=u.Pa)
        self.eta = Binder(value=eta, bounds=(inf, -inf), unit=u.Pa * u.s)
        super(Bingham, self).__init__(category=kwargs.pop('category', Category.BINGHAM), **kwargs)

    eta = Valid()
    r""":class:`.Valid`: Tangential viscosity :math:`\eta` in :math:`[M^{1} L^{-1} t^{-1}]`"""

    tau = Valid()
    r""":class:`.Valid`: Yield stress :math:`\tau` in :math:`[M^{1} L^{-1} t^{-2}]`"""

    _mat = ['name', 'sn', 'CAS', 'category', 'T', 'rho', 'p', 'c_p', 'v', 'gamma', 'M', 'm', 'V', 'nu', 'mu', 'Pr',
            'eta', 'tau']

    _Re = staticmethod(Re_bingham)
    r""" Calculate Reynolds with Re_bingham"""
