r"""
.. module:: MTIpython.materials
    :platform: Unix, Windows
    :synopsis: packages for materials

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from MTIpython.material import base, gas, granular, liquid, metal, plastic, principal, solid
from MTIpython.material.base import Category, Matter
from MTIpython.material.gas import Gas, HumidAir
from MTIpython.material.granular import Granular, Powder
from MTIpython.material.liquid import Bingham, Liquid
from MTIpython.material.materialfactory import material_factory
from MTIpython.material.metal import Metal
from MTIpython.material.plastic import Plastic
from MTIpython.material.solid import Solid

__all__ = ['base', 'solid', 'metal', 'plastic', 'liquid', 'gas', 'granular', 'Category', 'Matter',
           'Solid', 'Metal', 'Plastic', 'Liquid', 'Bingham', 'Gas', 'HumidAir', 'Granular',
           'Powder', 'principal', 'material_factory']
