r"""
.. module:: MTIpython.material.granular
    :platform: Unix, Windows
    :synopsis: Granular materials.

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from enum import Enum

from numpy import interp, array

from MTIpython.core.binder import Binder, Valid
from MTIpython.units.SI import u, isquantity, isiterable
from MTIpython.material.base import Category, Matter
from MTIpython.mtimath.types import inf

__all__ = ['Configuration', 'Granular', 'Powder', 'GeldartGrouping', 'ParticleSizeDistribution']


class ParticleSizeDistribution():
    r"""
    This object allows for the easy handling of Particle Size Distributions.

    Example usage:

        >>> import numpy as np
        >>> from MTIpython.material.granular import ParticleSizeDistribution
        >>> psd = ParticleSizeDistribution(size=np.array([0.325, 0.55, 1.1, 2.1]) * u.mm,fraction=np.array([0., 0.25, 0.75, 1.]))
        >>> psd()
        <Quantity([0.325 0.55  1.1   2.1  ], 'millimeter')>
        >>> d_50 = psd(fraction=0.5)
        >>> d_50
        <Quantity(0.825, 'millimeter')>
        >>> psd(fraction=[0.1, 0.5])
        <Quantity([0.415 0.825], 'millimeter')>
        >>> psd(size=0.95 * u.mm)
        <Quantity(0.6136363636363635, 'dimensionless')>
    """

    def __init__(self, size, fraction=None):
        r"""
        Particle Size Distribution.

        Args:
            size: either a magnitude or an array with the particle size(s) :math:`d` in :math:`[L^{1}]`
            fraction: fraction array, from :math:`0.0 ... 1.0` (Optional only used when size is an array)
        """
        if isiterable(size):
            fraction = fraction * u.dimensionless if not isquantity(fraction) else fraction
            if fraction is None or not isiterable(fraction):
                raise ValueError('Fraction should be an array between 0 ... 1 with the same length as size!')
            if len(fraction) != len(size):
                raise ValueError('Fraction and size should have the same length!')
            if not (0. <= fraction[0] <= 1.0) or not (0. <= fraction[-1] <= 1.0):
                raise ValueError('Fraction array should fall between 0 ... 1!')
            self.fraction = fraction
            self.size = size
        else:
            if isquantity(size):
                self.size = array([size.m]) * size.u
            else:
                self.size = array([size])
            self.fraction = array([1.]) * u.dimensionless

    def __call__(self, fraction=None, size=None):
        if fraction is not None:
            if isquantity(fraction):
                fraction = fraction.m
            return interp(fraction, self.fraction.m, self.size.m) * self.size.u
        elif size is not None:
            if isquantity(size):
                size = size.to(self.size.u).m
            return interp(size, self.size.m, self.fraction.m) * u.dimensionless
        else:
            return self.size if len(self.size) > 1 else self.size[0]

    def __repr__(self):
        return "PSD"

    fraction = None

    size = None


class GeldartGrouping(Enum):
    r"""
    In 1973, Professor D. Geldart proposed the grouping of powders in to four so-called "Geldart Groups". The groups are
    defined by their locations on a diagram of solid-fluid density difference and particle size. Design methods for
    fluidised beds can be tailored based upon the particle's Geldart grouping.

    .. figure:: resources/geldart.jpg
       :align: center
       :height: 200

    """

    A = 1
    r"""
    For this group the particle size is between :math:`20 ... 100 [\mu m]`, and the particle density is typically less
    than :math:`1400 \left[\frac{kg}{m^3}\right]`. Prior to the initiation of a bubbling bed phase, beds from these
    particles will expand by a factor of 2 to 3 at incipient fluidisation, due to a decreased bulk density. Most
    powder-catalyzed beds utilize this group.
    """

    B = 2
    r"""
    The particle size lies between :math:`40 ... 500 [\mu m]`, and the particle density between 
    :math:`1400 ... \left[\frac{kg}{m^3}\right]`. Bubbling typically forms directly at incipient fluidisation.
    """

    C = 3
    r"""
    This group contains extremely fine and consequently the most cohesive particles. With a size of 
    :math:`20 ... 30 [\mu m]` these particles fluidise under very difficult to achieve conditions, and may require the
    application of an external force, such as mechanical agitation.
    """

    D = 4
    r"""
    The particles in this region are above :math:`600 [\mu m]` and typically have high particle densities. Fluidisation
    of this group requires very high fluid energies and is typically associated with high levels of abrasion. Drying
    grains and peas, roasting coffee beans, gasifying coals, and some roasting metal ores are such solids, and they are
    usually processed in shallow beds or in the spouting mode.
    """


class Configuration(Enum):
    r"""
    Grain configurations
    """

    RLP = 1
    r"""
    RLP or Random Loose Packing is the lowest possible volume fraction obtainable
    """

    JAMMING = 2
    r"""
    The jammed state is one which each particle is in a mechanically stable equilibrium
    """

    RCP = 3
    r"""
    The RCP or Random Close Packing means the particles are touching and packed in as tightly as possible
    """


class Granular(Matter):
    def __init__(self, basematerial=None, configuration=None, PSD=None, **kwargs):
        r""" A granular material is a conglomeration of discrete solid, macroscopic particles. In some sense,
        granular materials do not constitute a single phase of matter but have characteristics reminiscent of solids,
        liquids, or gases depending on the average energy per grain. However, in each of these states granular materials
        also exhibit properties which are unique.

        Args:
            name (str): he common name for the material i.e. S235JR or Water
            rho (:class:`~pint:.Quantity` or :class:`.Binder`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
            T (:class:`~pint:.Quantity` or :class:`.Binder`): Temperature :math:`T` in :math:`[T]`
            c_p (:class:`~pint:.Quantity` or :class:`.Binder`): Specific heat at constant pressure :math:`c_p` in
             :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`~pint:.Quantity` or :class:`.Binder`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`~pint:.Quantity` or :class:`.Binder`): Mass :math:`m` in :math:`[M^{1}]`
            V (:class:`~pint:.Quantity` or :class:`.Binder`): Volume :math:`V` in :math:`[L^{3}]`
            v (:class:`~pint:.Quantity` or :class:`.Binder`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
            k (:class:`~pint:.Quantity` or :class:`.Binder`): Thermal conductivity :math:`k` in
             :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`~pint:.Quantity` or :class:`.Binder`): Specific weight :math:`\gamma` in
             :math:`[M^{1} L^{-2} t^{-2}]`
            basematerial (:class:`.Solid`): Base material
            configuration (:class:`.Configuration`): grain configuration
            PSD: Particle Size Distribution
            CAS: Chemical Abstracts Service number
            **kwargs: User set attributes

        Attributes:
            name (str): he common name for the material i.e. S235JR or Water
            sn (str): str: Short name for the material. When the :attr:`.name` consists of multiple  words, the short
             name is build from all first letters. When the name consist of a single word, the first two letters are
             used
            rho (:class:`.Valid`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`.
             rho is bound with (:attr:`.v`) or (:attr:`.gamma`) or (:attr:`.V` and :attr:`.m`) using :func:`.density`
             function
            T (:class:`.Valid`): Temperature :math:`T` in :math:`[T]`
            category (:class:`~Category`): The Material category
            c_p (:class:`.Valid`): Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`.Valid`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`.Valid`): Mass :math:`m` in :math:`[M^{1}]`
             m is bound with (:attr:`.rho` and :attr:`.V`) or (:attr:`.gamma` and :attr:`.v`) or (:attr:`.v` and
             :attr:`.V`) using :func:`.mass` function
            V (:class:`.Valid`): Volume :math:`V` in :math:`[L^{3}]`.
             V is bound with (:attr:`.rho` and :attr:`.m`) or (:attr:`.gamma` and :attr:`.m`) or (:attr:`.v` and
             :attr:`.m`) using :func:`.volume` function
            v (:class:`.Valid`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`.
             v is bound with (:attr:`.rho`) or (:attr:`.m` and :attr:`.V`) using :func:`.specific_volume` function
            k (:class:`.Valid`): Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`.Valid`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`.
             gamma is bound with (:attr:`.rho`) or (:attr:`.v`) or (:attr:`.m` and :attr:`.V`) using
             :func:`.specific_weight` function
            basematerial (:class:`.Solid`): Base material
            configuration (:class:`.granular.Configuration`): grain configuration
            PSD: Particle Size Distribution
            CAS: Chemical Abstracts Service number

        Note:
           Each creation triggers an info log entry, specifying the material specifications
           """
        self.basematerial = basematerial
        self.configuration = configuration
        self.PSD = PSD
        super(Granular, self).__init__(category=kwargs.pop('category', Category.GRANULAR), **kwargs)

    basematerial = None
    """:class:`MTIpython.material.solid.Solid` Base material"""

    PSD: ParticleSizeDistribution = None
    """:list(ndarray, ndarray): Particle Size Distribution"""

    configuration: Configuration = None
    """:class:`MTIpython.material.granular.Configuration` configuration of the powder"""

    geldart_group: GeldartGrouping = None

    _mat = ['name', 'sn', 'CAS', 'category', 'T', 'rho', 'c_p', 'v', 'k', 'gamma', 'M', 'm', 'V', 'PSD',
            'configuration', 'geldart_group', 'basematerial']
    """list(str): the variables which describe the state of the material. Override this list for each child """


class Powder(Granular):
    def __init__(self, Lambda=None, **kwargs):
        r""" A powder is a dry, bulk solid composed of a large number of very fine particles that may flow freely 
        when shaken or tilted. Powders are a special sub-class of granular materials, although the terms powder and 
        granular are sometimes used to distinguish separate classes of material. In particular, powders refer to 
        those granular materials that have the finer grain sizes, and that therefore have a greater tendency to form 
        clumps when flowing.
   
        Source: `Powder wiki <https://en.wikipedia.org/wiki/Powder_(substance)>`_
   
        Args:
            name (str): he common name for the material i.e. S235JR or Water
            rho (:class:`~pint:.Quantity` or :class:`.Binder`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
            T (:class:`~pint:.Quantity` or :class:`.Binder`): Temperature :math:`T` in :math:`[T]`
            c_p (:class:`~pint:.Quantity` or :class:`.Binder`): Specific heat at constant pressure :math:`c_p` in
             :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`~pint:.Quantity` or :class:`.Binder`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`~pint:.Quantity` or :class:`.Binder`): Mass :math:`m` in :math:`[M^{1}]`
            V (:class:`~pint:.Quantity` or :class:`.Binder`): Volume :math:`V` in :math:`[L^{3}]`
            v (:class:`~pint:.Quantity` or :class:`.Binder`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
            k (:class:`~pint:.Quantity` or :class:`.Binder`): Thermal conductivity :math:`k` in
             :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`~pint:.Quantity` or :class:`.Binder`): Specific weight :math:`\gamma` in
             :math:`[M^{1} L^{-2} t^{-2}]`
            basematerial (:class:`.Solid`): Base material
            configuration (:class:`.Configuration`): grain configuration
            PSD: Particle Size Distribution
            Lambda (:class:`~pint:.Quantity` or :class:`.Binder`): Progress resistance coefficient :math:`\lambda` in
             :math:`[-]`
            CAS: Chemical Abstracts Service number
            **kwargs: User set attributes

        Attributes:
            name (str): he common name for the material i.e. S235JR or Water
            sn (str): str: Short name for the material. When the :attr:`.name` consists of multiple  words, the short
             name is build from all first letters. When the name consist of a single word, the first two letters are
             used
            rho (:class:`.Valid`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`.
             rho is bound with (:attr:`.v`) or (:attr:`.gamma`) or (:attr:`.V` and :attr:`.m`) using :func:`.density`
             function
            T (:class:`.Valid`): Temperature :math:`T` in :math:`[T]`
            category (:class:`~Category`): The Material category
            c_p (:class:`.Valid`): Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`.Valid`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`.Valid`): Mass :math:`m` in :math:`[M^{1}]`
             m is bound with (:attr:`.rho` and :attr:`.V`) or (:attr:`.gamma` and :attr:`.v`) or (:attr:`.v` and
             :attr:`.V`) using :func:`.mass` function
            V (:class:`.Valid`): Volume :math:`V` in :math:`[L^{3}]`.
             V is bound with (:attr:`.rho` and :attr:`.m`) or (:attr:`.gamma` and :attr:`.m`) or (:attr:`.v` and
             :attr:`.m`) using :func:`.volume` function
            v (:class:`.Valid`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`.
             v is bound with (:attr:`.rho`) or (:attr:`.m` and :attr:`.V`) using :func:`.specific_volume` function
            k (:class:`.Valid`): Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`.Valid`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`.
             gamma is bound with (:attr:`.rho`) or (:attr:`.v`) or (:attr:`.m` and :attr:`.V`) using
             :func:`.specific_weight` function
            basematerial (:class:`.Solid`): Base material
            configuration (:class:`.granular.Configuration`): grain configuration
            PSD: Particle Size Distribution
            Lambda (:class:`.Valid`): Progress resistance coefficient :math:`\lambda` in :math:`[-]`
            CAS: Chemical Abstracts Service number

        Note:
           Each creation triggers an info log entry, specifying the material specifications
           """
        self.Lambda = Binder(value=Lambda, bounds=(0., inf))
        super(Powder, self).__init__(category=kwargs.pop('category', Category.POWDER), **kwargs)

    Lambda = Valid()
    """:class:`.Valid`: (:class:`~pint:.Quantity`): Progress resistance coefficient :math:`\lambda` in :math:`[-]`"""

    _mat = ['name', 'sn', 'CAS', 'category', 'T', 'rho', 'c_p', 'v', 'k', 'gamma', 'M', 'm', 'V', 'PSD',
            'configuration', 'geldart_group', 'Lambda', 'basematerial']
    """list(str): the variables which describe the state of the material. Override this list for each child """
