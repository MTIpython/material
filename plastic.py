r"""
.. module:: MTIpython.material.plastic
    :platform: Unix, Windows
    :synopsis: Plastic materials.

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from MTIpython.core.binder import Binder, Valid
from MTIpython.material.base import Category
from MTIpython.material.solid import Solid
from MTIpython.mtimath.types import inf
from MTIpython.units.SI import u

__all__ = ['Plastic']


class Plastic(Solid):
    def __init__(self, epsilon_m=None, sigma_M=None, T_use=None, **kwargs):
        r"""
        Plastic is material consisting of any of a wide range of synthetic or semi-synthetic organic compounds that are
        malleable and so can be molded into solid objects.

        Source: `wikipedia <https://en.wikipedia.org/wiki/Plastic>`_

        Args:
            name (str): he common name for the material i.e. S235JR or Water
            rho (:class:`~pint:.Quantity` or :class:`.Binder`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
            T (:class:`~pint:.Quantity` or :class:`.Binder`): Temperature :math:`T` in :math:`[T]`
            c_p (:class:`~pint:.Quantity` or :class:`.Binder`): Specific heat at constant pressure :math:`c_p` in
             :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`~pint:.Quantity` or :class:`.Binder`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`~pint:.Quantity` or :class:`.Binder`): Mass :math:`m` in :math:`[M^{1}]`
            V (:class:`~pint:.Quantity` or :class:`.Binder`): Volume :math:`V` in :math:`[L^{3}]`
            v (:class:`~pint:.Quantity` or :class:`.Binder`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
            k (:class:`~pint:.Quantity` or :class:`.Binder`): Thermal conductivity :math:`k` in
             :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`~pint:.Quantity` or :class:`.Binder`): Specific weight :math:`\gamma` in
             :math:`[M^{1} L^{-2} t^{-2}]`
            E (:class:`~pint:.Quantity` or :class:`.Binder`): Elastic modulus :math:`E` in :math:`[M^{1} L^{-1} t^{-2}]`
            epsilon (:class:`~pint:.Quantity` or :class:`.Binder`): Surface roughness :math:`\varepsilon` in
             :math:`[L^{1}]`
            rel_cost (:class:`~pint:.Quantity` or :class:`.Binder`): Relative material cost, compared with steel S235JR
             in :math:`\left[-\right]`, these values can be obtained from : Muhs, Dieter, and Wilhelm Matek.
             Roloff/Matek machine-onderdelen: tabellenboek. SDU Uitgevers, n.d.
            epsilon_m (:class:`~pint:.Quantity` or :class:`.Binder`): Elongation :math:`\varepsilon_{m}` in :math:`[\%]`
            sigma_M (:class:`~pint:.Quantity` or :class:`.Binder`): Tensile strength :math:`\sigma_{M}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            T_use (:class:`~pint:.Quantity` or :class:`.Binder`): Allowable usage temperature
             :math:`T_{use} = [T_{min}, T_{max}]` in :math:`[T^{1}]`
            CAS: Chemical Abstracts Service number
            **kwargs: User set attributes

        Attributes:
            name (str): he common name for the material i.e. S235JR or Water
            sn (str): str: Short name for the material. When the :attr:`.name` consists of multiple  words, the short
             name is build from all first letters. When the name consist of a single word, the first two letters are
             used
            rho (:class:`.Valid`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`.
             rho is bound with (:attr:`.v`) or (:attr:`.gamma`) or (:attr:`.V` and :attr:`.m`) using :func:`.density`
             function
            T (:class:`.Valid`): Temperature :math:`T` in :math:`[T]`
            category (:class:`~Category`): The Material category
            c_p (:class:`.Valid`): Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`.Valid`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`.Valid`): Mass :math:`m` in :math:`[M^{1}]`
             m is bound with (:attr:`.rho` and :attr:`.V`) or (:attr:`.gamma` and :attr:`.v`) or (:attr:`.v` and
             :attr:`.V`) using :func:`.mass` function
            V (:class:`.Valid`): Volume :math:`V` in :math:`[L^{3}]`.
             V is bound with (:attr:`.rho` and :attr:`.m`) or (:attr:`.gamma` and :attr:`.m`) or (:attr:`.v` and
             :attr:`.m`) using :func:`.volume` function
            v (:class:`.Valid`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`.
             v is bound with (:attr:`.rho`) or (:attr:`.m` and :attr:`.V`) using :func:`.specific_volume` function
            k (:class:`.Valid`): Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`.Valid`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`.
             gamma is bound with (:attr:`.rho`) or (:attr:`.v`) or (:attr:`.m` and :attr:`.V`) using
             :func:`.specific_weight` function
            E (:class:`.Valid`): Elastic modulus :math:`E` in :math:`[M^{1} L^{-1} t^{-2}]`
            epsilon (:class:`.Valid`): Surface roughness :math:`\varepsilon` in :math:`[L^{1}]`
            rel_cost (:class:`.Valid`): Relative material cost, compared with steel S235JR
             in :math:`\left[-\right]`, these values can be obtained from : Muhs, Dieter, and Wilhelm Matek.
             Roloff/Matek machine-onderdelen: tabellenboek. SDU Uitgevers, n.d.
            sigma_M (:class:`.Valid`): Tensile strength :math:`\sigma_{M}` in :math:`[M^{1} L^{-1} t^{-2}]`
            T_use (:class:`.Valid`): Allowable usage temperature :math:`T_{use} = [T_{min}, T_{max}]` in :math:`[T^{1}]`
            CAS: Chemical Abstracts Service number

        Note:
           Each creation triggers an info log entry, specifying the material specifications
        """
        self.sigma_M = Binder(value=sigma_M, bounds=(-inf, inf), unit=u.MPa)
        self.T_use = Binder(value=T_use, bounds=(0., inf), unit=u.K)
        self.epsilon_m = Binder(value=epsilon_m, bounds=(0., 100.), unit=u.dimensionless)
        super(Plastic, self).__init__(category=Category.PLASTIC, **kwargs)

    sigma_M = Valid()
    r""":class:`.Valid`: Tensile strength :math:`\sigma_{M}` in :math:`[M^{1} L^{-1} t^{-2}]`"""

    T_use = Valid()
    r""":class:`.Valid`: Allowable usage temperature :math:`T_{use} = [T_{min}, T_{max}]` in 
    :math:`[T^{1}]`"""

    epsilon_m = Valid()
    r""":class:`.Valid`: Elongation :math:`\varepsilon_{m}` in :math:`[%]`"""

    _mat = ['name', 'sn', 'CAS', 'category', 'T', 'rho', 'c_p', 'v', 'gamma', 'M', 'm', 'V', 'E', 'epsilon', 'rel_cost',
             'sigma_M', 'T_use', 'epsilon_m']
    """list(str): the variables which describe the state of the material. Override this list for each child """
