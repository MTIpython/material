r"""
.. module:: MTIpython.material.metal
    :platform: Unix, Windows
    :synopsis: Metallic materials.

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from MTIpython.core.binder import Binder, Valid
from MTIpython.material.base import Category
from MTIpython.material.solid import Solid
from MTIpython.mtimath.types import inf
from MTIpython.units.SI import u

__all__ = ['Metal']


class Metal(Solid):
    def __init__(self, G=None, R_mN=None, R_eN=None, sigma_tdWN=None, sigma_bWN=None, tau_tWN=None, **kwargs):
        r"""
        A metal (from Greek μέταλλον métallon, "mine, quarry, metal") is a material (an element, compound, or
        alloy) that is typically hard when in solid state, opaque, shiny, and has good electrical and thermal
        conductivity. Metals are generally malleable—that is, they can be hammered or pressed permanently out of shape
        without breaking or cracking—as well as fusible (able to be fused or melted) and ductile (able to be drawn out
        into a thin wire). Around 90 of the 118 elements in the periodic table are metals;

        Source: `wikipedia <https://en.wikipedia.org/wiki/Metal>`_

        Args:
            name (str): he common name for the material i.e. S235JR or Water
            rho (:class:`~pint:.Quantity` or :class:`.Binder`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
            T (:class:`~pint:.Quantity` or :class:`.Binder`): Temperature :math:`T` in :math:`[T]`
            c_p (:class:`~pint:.Quantity` or :class:`.Binder`): Specific heat at constant pressure :math:`c_p` in
             :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`~pint:.Quantity` or :class:`.Binder`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`~pint:.Quantity` or :class:`.Binder`): Mass :math:`m` in :math:`[M^{1}]`
            V (:class:`~pint:.Quantity` or :class:`.Binder`): Volume :math:`V` in :math:`[L^{3}]`
            v (:class:`~pint:.Quantity` or :class:`.Binder`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
            k (:class:`~pint:.Quantity` or :class:`.Binder`): Thermal conductivity :math:`k` in
             :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`~pint:.Quantity` or :class:`.Binder`): Specific weight :math:`\gamma` in
             :math:`[M^{1} L^{-2} t^{-2}]`
            E (:class:`~pint:.Quantity` or :class:`.Binder`): Elastic modulus :math:`E` in :math:`[M^{1} L^{-1} t^{-2}]`
            epsilon (:class:`~pint:.Quantity` or :class:`.Binder`): Surface roughness :math:`\varepsilon` in
             :math:`[L^{1}]`
            rel_cost (:class:`~pint:.Quantity` or :class:`.Binder`): Relative material cost, compared with steel S235JR
             in :math:`\left[-\right]`, these values can be obtained from : Muhs, Dieter, and Wilhelm Matek.
             Roloff/Matek machine-onderdelen: tabellenboek. SDU Uitgevers, n.d.
            G (:class:`~pint:.Quantity` or :class:`.Binder`): Shear modulus :math:`G` in :math:`[M^{1} L^{-1} t^{-2}]`
            R_mN (:class:`~pint:.Quantity` or :class:`.Binder`): Ultimate strength :math:`R_{mN}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            R_eN (:class:`~pint:.Quantity` or :class:`.Binder`): Yield strength :math:`R_{eN}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            sigma_tdWN (:class:`~pint:.Quantity` or :class:`.Binder`): Flexural strength torsion :math:`\sigma_{tdWN}`
             in :math:`[M^{1} L^{-1} t^{-2}]`
            sigma_bWN (:class:`~pint:.Quantity` or :class:`.Binder`): Flexural strength bending :math:`\sigma_{bWN}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            tau_tWN (:class:`~pint:.Quantity` or :class:`.Binder`): Shear strength :math:`\tau_{tWN}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            CAS: Chemical Abstracts Service number
            **kwargs: User set attributes

        Attributes:
            name (str): he common name for the material i.e. S235JR or Water
            sn (str): str: Short name for the material. When the :attr:`.name` consists of multiple  words, the short
             name is build from all first letters. When the name consist of a single word, the first two letters are
             used
            rho (:class:`.Valid`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`.
             rho is bound with (:attr:`.v`) or (:attr:`.gamma`) or (:attr:`.V` and :attr:`.m`) using :func:`.density`
             function
            T (:class:`.Valid`): Temperature :math:`T` in :math:`[T]`
            category (:class:`~Category`): The Material category
            c_p (:class:`.Valid`): Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`.Valid`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`.Valid`): Mass :math:`m` in :math:`[M^{1}]`
             m is bound with (:attr:`.rho` and :attr:`.V`) or (:attr:`.gamma` and :attr:`.v`) or (:attr:`.v` and
             :attr:`.V`) using :func:`.mass` function
            V (:class:`.Valid`): Volume :math:`V` in :math:`[L^{3}]`.
             V is bound with (:attr:`.rho` and :attr:`.m`) or (:attr:`.gamma` and :attr:`.m`) or (:attr:`.v` and
             :attr:`.m`) using :func:`.volume` function
            v (:class:`.Valid`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`.
             v is bound with (:attr:`.rho`) or (:attr:`.m` and :attr:`.V`) using :func:`.specific_volume` function
            k (:class:`.Valid`): Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`.Valid`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`.
             gamma is bound with (:attr:`.rho`) or (:attr:`.v`) or (:attr:`.m` and :attr:`.V`) using
             :func:`.specific_weight` function
            E (:class:`.Valid`): Elastic modulus :math:`E` in :math:`[M^{1} L^{-1} t^{-2}]`
            epsilon (:class:`.Valid`): Surface roughness :math:`\varepsilon` in :math:`[L^{1}]`
            rel_cost (:class:`.Valid`): Relative material cost, compared with steel S235JR
             in :math:`\left[-\right]`, these values can be obtained from : Muhs, Dieter, and Wilhelm Matek.
             Roloff/Matek machine-onderdelen: tabellenboek. SDU Uitgevers, n.d.
            G (:class:`.Valid`): Shear modulus :math:`G` in :math:`[M^{1} L^{-1} t^{-2}]`
            R_mN (:class:`.Valid`): Ultimate strength :math:`R_{mN}` in :math:`[M^{1} L^{-1} t^{-2}]`
            R_eN (:class:`.Valid`): Yield strength :math:`R_{eN}` in :math:`[M^{1} L^{-1} t^{-2}]`
            sigma_tdWN (:class:`.Valid`): Flexural strength torsion :math:`\sigma_{tdWN}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            sigma_bWN (:class:`.Valid`): Flexural strength bending :math:`\sigma_{bWN}` in :math:`[M^{1} L^{-1} t^{-2}]`
            tau_tWN (:class:`.Valid`): Shear strength :math:`\tau_{tWN}` in :math:`[M^{1} L^{-1} t^{-2}]`
            CAS: Chemical Abstracts Service number

        Note:
           Each creation triggers an info log entry, specifying the material specifications

        Todo:
            Check if metal class can accommodate Steels, Coppers, Aluminium etc. For now the focus is on steel
        """
        self.G = Binder(value=G, bounds=(-inf, inf), unit=u.MPa)
        self.R_mN = Binder(value=R_mN, bounds=(-inf, inf), unit=u.MPa)
        self.R_eN = Binder(value=R_eN, bounds=(-inf, inf), unit=u.MPa)
        self.sigma_tdWN = Binder(value=sigma_tdWN, bounds=(-inf, inf), unit=u.MPa)
        self.sigma_bWN = Binder(value=sigma_bWN, bounds=(-inf, inf), unit=u.MPa)
        self.tau_tWN = Binder(value=tau_tWN, bounds=(-inf, inf), unit=u.MPa)
        super(Metal, self).__init__(category=Category.METAL, **kwargs)

    G = Valid()
    r""":class:`.Valid`: Shear modulus :math:`G` in :math:`[M^{1} L^{-1} t^{-2}]`"""

    R_eN = Valid()
    r""":class:`.Valid`: Yield strength :math:`R_{eN}` in :math:`[M^{1} L^{-1} t^{-2}]`"""

    R_mN = Valid()
    r""":class:`.Valid`: Ultimate strength :math:`R_{mN}` in :math:`[M^{1} L^{-1} t^{-2}]`"""

    sigma_bWN = Valid()
    r""":class:`.Valid`: Flexural strength bending :math:`\sigma_{bWN}` in :math:`[M^{1} L^{-1} 
    t^{-2}]`"""

    sigma_tdWN = Valid()
    r""":class:`.Valid`: Flexural strength torsion :math:`\sigma_{tdWN}` in :math:`[M^{1} L^{
    -1} t^{-2}]`"""

    tau_tWN = Valid()
    r""":class:`.Valid`: Shear strength :math:`\tau_{tWN}` in :math:`[M^{1} L^{-1} t^{-2}]`"""

    _mat = ['name', 'sn', 'CAS', 'category', 'T', 'rho', 'c_p', 'v', 'k', 'gamma', 'M', 'm', 'V', 'E', 'epsilon',
            'rel_cost', 'G', 'R_eN', 'R_mN', 'sigma_bWN', 'sigma_tdWN', 'tau_tWN']
    """list(str): the variables which describe the state of the material. Override this list for each child """
