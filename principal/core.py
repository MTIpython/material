r"""
.. module:: MTIpython.material.principal
    :platform: Unix, Windows
    :synopsis: Universal functions, relating to materials

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from CoolProp.CoolProp import PropsSI as propsi
from numpy import vectorize

from MTIpython.core.functions import function_switch
from MTIpython.reference.bibliography import citation
from MTIpython.units.SI import arequantities, isquantity, u

PropsSI = vectorize(propsi)

__all__ = ['MTICoolprop', 'dynamic_viscosity', 'kinematic_viscosity', 'specific_volume', 'specific_weight', 'density',
           'mass', 'volume', 'enthalpy']

class MTICoolprop:
    source = r"""
        @article{doi:10.1021/ie4033999,
        author = {Bell, Ian H. and Wronski, Jorrit and Quoilin, Sylvain and Lemort, Vincent},
        title = {Pure and Pseudo-pure Fluid Thermophysical Property Evaluation and
                 the Open-Source Thermophysical Property Library CoolProp},
        journal = {Industrial \& Engineering Chemistry Research},
        volume = {53},
        number = {6},
        pages = {2498--2508},
        year = {2014},
        doi = {10.1021/ie4033999},
        URL = {http://pubs.acs.org/doi/abs/10.1021/ie4033999},
        eprint = {http://pubs.acs.org/doi/pdf/10.1021/ie4033999}
    }
    """

    @staticmethod
    @citation(source)
    def density(material, T, p):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return PropsSI('D', 'T', T.to('K').m, 'P', p.to('Pa').m, material) * u.kg / u.m ** 3

    @staticmethod
    @citation(source)
    def pressure(material, T, rho):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            rho (:class:`~pint:.Quantity`): Density :math:`\rho`in :math:`\left[\frac{kg}{m^3}\right]`

        Returns:
            :class:`~pint:.Quantity`: Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
        """
        return PropsSI('P', 'T', T.to('K').m, 'D', rho.to('kg/m**3').m, material) * u.Pa

    @staticmethod
    @citation(source)
    def temperature(material, p, rho):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
            rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`

        Returns:
            :class:`~pint:.Quantity`: Temperature :math:`T` in :math:`[T]`
        """
        return PropsSI('T', 'P', p.to('Pa').m, 'D', rho.to('kg/m**3').m, material) * u.K

    @staticmethod
    @citation(source)
    def compressibility_factor(material, p, T):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`

        Returns:
            :class:`~pint:.Quantity`: Compressibility factor :math:`Z` in :math:`[-]`
        """
        return PropsSI('Z', 'P', p.to('Pa').m, 'T', T.to('K').m, material) * u.dimensionless

    @staticmethod
    @citation(source)
    def viscosity(material, T, p):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
        """
        return PropsSI('V', 'T', T.to('K').m, 'P', p.to('Pa').m, material) * u.Pa * u.s

    @staticmethod
    @citation(source)
    def conductivity(material, T, p):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
        """
        return PropsSI('L', 'T', T.to('K').m, 'P', p.to('Pa').m, material) * u.W / (u.m * u.K)

    @staticmethod
    @citation(source)
    def heat_capacity(material, T, p):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Heat capacity :math:`c_p` at constant pressure in :math:`[L^{2} T^{-1} t^{-2}]`
        """
        return PropsSI('C', 'T', T.to('K').m, 'P', p.to('Pa').m, material) * u.J / (u.kg * u.K)

    @staticmethod
    @citation(source)
    def molar_mass(material):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name

        Returns:
            :class:`~pint:.Quantity`: Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
        """
        return PropsSI('molar_mass', material) * u.kg / u.mol

    @staticmethod
    @citation(source)
    def prandtl(material, T, p):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`


        Returns:
            :class:`~pint:.Quantity`: Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
        """
        return PropsSI('PRANDTL', 'T', T.to('K').m, 'P', p.to('Pa').m, material) * u.dimensionless

@citation(r"""
@book{white_fluid_2011,
    title = {Fluid {Mechanics}},
    isbn = {978-0-07-352934-9},
    publisher = {McGraw Hill},
    author = {White, Frank M.},
    year = {2011}
}""")
def kinematic_viscosity(rho, mu):
    r"""
    Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]` expressed in its resistance to shearing flows, where
    adjacent layers move parallel to each other with different speeds. Calculated with

    .. math::
       \nu = \frac{\mu}{\rho}

    Args:
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        mu (:class:`~pint:.Quantity`): Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`

    Returns:
        :class:`~pint:.Quantity`: Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`
    """
    return mu / rho


@citation(r"""
@book{white_fluid_2011,
    title = {Fluid {Mechanics}},
    isbn = {978-0-07-352934-9},
    publisher = {McGraw Hill},
    author = {White, Frank M.},
    year = {2011}
}""")
def dynamic_viscosity(rho, nu):
    r"""
    Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]` expressed in its resistance to shearing flows, where
    adjacent layers move parallel to each other with different speeds. Calculated with

    .. math::
       \mu = \nu \rho

    Args:
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        nu (:class:`~pint:.Quantity`): Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`

    Returns:
        :class:`~pint:.Quantity`: Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
    """
    return nu * rho


def specific_volume(**kwargs):
    r"""
    The specific volume :math:`v`, in :math:`[M^{-1} L^{3}]`,

    Obtained with the inverse of density

    .. math::
       \gamma = \rho^{-1}

    .. math::
       \gamma = \frac{V}{m}

    Args:
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^1 L^{-3}]`
        m (:class:`~pint:.Quantity`): Mass :math:`m` in :math:`[M^{1}]`
        V (:class:`~pint:.Quantity`): Volume :math:`V` in :math:`[L^{3}]`

        Kwargs Combinations:
            * rho
            * m, V

    Returns:
        :class:`~pint:.Quantity`: Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
    """
    return function_switch(specific_volume_from_density,
                           specific_volume_from_mass_and_volume,
                           **kwargs)


@citation(r"""
@book{white_fluid_2011,
    title = {Fluid {Mechanics}},
    isbn = {978-0-07-352934-9},
    publisher = {McGraw Hill},
    author = {White, Frank M.},
    year = {2011}
}""")
def specific_volume_from_density(rho):
    r"""
    The specific volume :math:`v`, in :math:`[M^{-1} L^{3}]`,

    Obtained with the inverse of density

    .. math::
       \gamma = \rho^{-1}

    Args:
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^1 L^{-3}]`

    Returns:
        :class:`~pint:.Quantity`: Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
    """
    return rho ** -1


@citation(r"""
@book{white_fluid_2011,
    title = {Fluid {Mechanics}},
    isbn = {978-0-07-352934-9},
    publisher = {McGraw Hill},
    author = {White, Frank M.},
    year = {2011}
}""")
def specific_volume_from_mass_and_volume(m, V):
    r"""
    The specific volume :math:`v`, in :math:`[M^{-1} L^{3}]`,

    Obtained with volume divided by mass

    .. math::
       \gamma = \frac{V}{m}

    Args:
        m (:class:`~pint:.Quantity`): Mass :math:`m` in :math:`[M^{1}]`
        V (:class:`~pint:.Quantity`): Volume :math:`V` in :math:`[L^{3}]`

    Returns:
        :class:`~pint:.Quantity`: Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
    """
    return V / m


def specific_weight(**kwargs):
    r"""
    The specific weight :math:`\gamma`, (also known as the unit weight) in :math:`[M^1 L^{-2} T^{-2}]`, obtained with:

    .. math::
       \gamma = \rho g_0

    .. math::
       \gamma = v^{-1} g_0

    .. math::
       \gamma = \frac{m}{V} g_0

    Args:
        v (:class:`~pint:.Quantity`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^1 L^{-3}]`
        m (:class:`~pint:.Quantity`): Mass :math:`m` in :math:`[M^{1}]`
        V (:class:`~pint:.Quantity`): Volume :math:`V` in :math:`[L^{3}]`

    Kwargs Combinations:
        * v
        * rho
        * m, V

    Returns:
        :class:`~pint:.Quantity`: Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`
    """
    return function_switch(specific_weight_from_density,
                           specific_weight_from_specific_volume,
                           specific_weight_from_mass_and_volume,
                           **kwargs)


@citation(r"""
@book{white_fluid_2011,
    title = {Fluid {Mechanics}},
    isbn = {978-0-07-352934-9},
    publisher = {McGraw Hill},
    author = {White, Frank M.},
    year = {2011}
}""")
def specific_weight_from_density(rho):
    r"""
    The specific weight :math:`\gamma`, (also known as the unit weight) in :math:`[M^1 L^{-2} T^{-2}]`, obtained with:

    .. math::
       \gamma = \rho g_0

    Args:
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^1 L^{-3}]`

    Returns:
        :class:`~pint:.Quantity`: Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`
    """
    return rho * u.g_0 if isquantity(rho) else rho * u.g_0.m


@citation(r"""
@book{white_fluid_2011,
    title = {Fluid {Mechanics}},
    isbn = {978-0-07-352934-9},
    publisher = {McGraw Hill},
    author = {White, Frank M.},
    year = {2011}
}""")
def specific_weight_from_specific_volume(v):
    r"""
    The specific weight :math:`\gamma`, (also known as the unit weight) in :math:`[M^1 L^{-2} T^{-2}]`, obtained with:

    .. math::
       \gamma = v^{-1} g_0

    Args:
        v (:class:`~pint:.Quantity`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`

    Returns:
        :class:`~pint:.Quantity`: Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`
    """
    return u.g_0 / v if isquantity(v) else u.g_0.m / v


@citation(r"""
@book{white_fluid_2011,
    title = {Fluid {Mechanics}},
    isbn = {978-0-07-352934-9},
    publisher = {McGraw Hill},
    author = {White, Frank M.},
    year = {2011}
}""")
def specific_weight_from_mass_and_volume(m, V):
    r"""
    The specific weight :math:`\gamma`, (also known as the unit weight) in :math:`[M^1 L^{-2} T^{-2}]`, obtained with:

    .. math::
       \gamma = \frac{m}{V} g_0

    Args:
        v (:class:`~pint:.Quantity`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`

    Returns:
        :class:`~pint:.Quantity`: Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`
    """
    return m * u.g_0 / V if arequantities(m, V) else m * u.g_0.m / V


def density(**kwargs):
    r"""
    The density :math:`\rho` in :math:`[M^1 L^{-3}]`, obtained with

    .. math::
       \rho = v^{-1}

    .. math::
       \rho = \frac{\gamma}{g_0}

    .. math::
       \rho = \frac{m}{V}

    Args:
        v (:class:`~pint:.Quantity`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
        gamma (:class:`~pint:.Quantity`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        V (:class:`~pint:.Quantity`): Volume :math:`V` in :math:`[L^{3}]`

    Kwargs Combinations:
        * v
        * gamma
        * m, V

    Returns:
        :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^1 L^{-3}]`
    """
    return function_switch(density_from_specific_volume,
                           density_from_specific_weight,
                           density_from_mass_and_volume,
                           **kwargs)


@citation(r"""
@book{white_fluid_2011,
    title = {Fluid {Mechanics}},
    isbn = {978-0-07-352934-9},
    publisher = {McGraw Hill},
    author = {White, Frank M.},
    year = {2011}
}""")
def density_from_specific_volume(v):
    r"""
    The density :math:`\rho` in :math:`[M^1 L^{-3}]`, obtained with

    .. math::
       \rho = v^{-1}

    Args:
        v (:class:`~pint:.Quantity`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`

    Returns:
        :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^1 L^{-3}]`
    """
    return v ** -1


@citation(r"""
@book{white_fluid_2011,
    title = {Fluid {Mechanics}},
    isbn = {978-0-07-352934-9},
    publisher = {McGraw Hill},
    author = {White, Frank M.},
    year = {2011}
}""")
def density_from_specific_weight(gamma):
    r"""
    The density :math:`\rho` in :math:`[M^1 L^{-3}]`, obtained with

    .. math::
       \rho = \frac{\gamma}{g_0}

    Args:
        gamma (:class:`~pint:.Quantity`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`

    Returns:
        :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^1 L^{-3}]`
    """
    return gamma / u.g_0 if isquantity(gamma) else gamma / u.g_0.m


def density_from_mass_and_volume(m, V):
    r"""
    The density :math:`\rho` in :math:`[M^1 L^{-3}]`, obtained with

    .. math::
       \rho = \frac{m}{V}

    Args:
        m (:class:`~pint:.Quantity`): Mass :math:`m` in :math:`[M^{1}]`
        V (:class:`~pint:.Quantity`): Volume :math:`V` in :math:`[L^{3}]`

    Returns:
        :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^1 L^{-3}]`
    """
    return m / V


def mass(**kwargs):
    r"""
    Mass :math:`m` in :math:`[M^1]`, obtained with

    .. math::
       m = \rho V

    .. math::
       m = \frac{\gamma V}{g_0}

    .. math::
       m = \frac{V}{v}

    Args:
        v (:class:`~pint:.Quantity`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
        gamma (:class:`~pint:.Quantity`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^1 L^{-3}]`
        V (:class:`~pint:.Quantity`): Volume :math:`V` in :math:`[L^{3}]`

    Kwargs Combinations:
        * rho, V
        * gamma, V
        * v, V

    Returns:
        :class:`~pint:.Quantity`: Mass :math:`m` in :math:`[M^{1}]`
    """
    return function_switch(mass_from_density_and_volume,
                           mass_from_specific_volume_and_volume,
                           mass_from_specific_weight_and_volume,
                           **kwargs)


def mass_from_density_and_volume(rho, V):
    r"""
    Mass :math:`m` in :math:`[M^1]`, obtained with

    .. math::
       m = \rho V

    Args:
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^1 L^{-3}]`
        V (:class:`~pint:.Quantity`): Volume :math:`V` in :math:`[L^{3}]`

    Returns:
        :class:`~pint:.Quantity`: Mass :math:`m` in :math:`[M^{1}]`
    """
    return rho * V


def mass_from_specific_weight_and_volume(gamma, V):
    r"""
    Mass :math:`m` in :math:`[M^1]`, obtained with

    .. math::
       m = \frac{\gamma V}{g_0}

    Args:
        gamma (:class:`~pint:.Quantity`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`
        V (:class:`~pint:.Quantity`): Volume :math:`V` in :math:`[L^{3}]`

    Returns:
        :class:`~pint:.Quantity`: Mass :math:`m` in :math:`[M^{1}]`
    """
    return gamma * V / u.g_0 if arequantities(gamma, V) else gamma * V / u.g_0.m


def mass_from_specific_volume_and_volume(v, V):
    r"""
    Mass :math:`m` in :math:`[M^1]`, obtained with

    .. math::
       m = \frac{V}{v}

    Args:
        v (:class:`~pint:.Quantity`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
        V (:class:`~pint:.Quantity`): Volume :math:`V` in :math:`[L^{3}]`

    Returns:
        :class:`~pint:.Quantity`: Mass :math:`m` in :math:`[M^{1}]`
    """
    return V / v


def volume(**kwargs):
    r"""
    Volume :math:`V` in :math:`[L^3]`, obtained with

    .. math::
       V = \frac{m}{\rho}

    .. math::
       V = v m

    .. math::
       V = \frac{m g_0}{\gamma}

    Args:
        v (:class:`~pint:.Quantity`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^1 L^{-3}]`
        m (:class:`~pint:.Quantity`): Mass :math:`m` in :math:`[M^{1}]`
        gamma (:class:`~pint:.Quantity`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`

    Kwargs Combinations:
        * m, rho
        * m, v
        * m, gamma

    Returns:
        :class:`~pint:.Quantity`: Volume :math:`V` in :math:`[L^{3}]`
    """
    return function_switch(volume_from_density_and_mass,
                           volume_from_specific_volume_and_mass,
                           volume_from_specific_weight_and_mass,
                           **kwargs)


def volume_from_density_and_mass(rho, m):
    r"""
    Volume :math:`V` in :math:`[L^3]`, obtained with

    .. math::
       V = \frac{m}{\rho}

    Args:
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^1 L^{-3}]`
        m (:class:`~pint:.Quantity`): Mass :math:`m` in :math:`[M^{1}]`

    Returns:
        :class:`~pint:.Quantity`: Volume :math:`V` in :math:`[L^{3}]`
    """
    return m / rho


def volume_from_specific_volume_and_mass(v, m):
    r"""
    Volume :math:`V` in :math:`[L^3]`, obtained with

    .. math::
       V = v m

    Args:
        v (:class:`~pint:.Quantity`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
        m (:class:`~pint:.Quantity`): Mass :math:`m` in :math:`[M^{1}]`

    Returns:
        :class:`~pint:.Quantity`: Volume :math:`V` in :math:`[L^{3}]`
    """
    return v * m


def volume_from_specific_weight_and_mass(gamma, m):
    r"""
    Volume :math:`V` in :math:`[L^3]`, obtained with

    .. math::
       V = \frac{m g_0}{\gamma}

    Args:
        m (:class:`~pint:.Quantity`): Mass :math:`m` in :math:`[M^{1}]`
        gamma (:class:`~pint:.Quantity`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`

    Returns:
        :class:`~pint:.Quantity`: Volume :math:`V` in :math:`[L^{3}]`
    """
    return m * u.g_0 / gamma if arequantities(gamma, m) else m * u.g_0.m / gamma

def enthalpy_from_specific_enthalpy(h, m):
    r"""
    Calculate the enthalpy :math:`H` in :math:`[M^{1} L^{2} t^{-2}]` from the specific enthalpy :math:`h`

    .. math::
       H = h m

    Args:
        h: Specific enthalpy :math:`h` in :math:`[L^{2} t^{-2}]`
        m: mass :math:`m` in :math:`[M^{1}]`

    Returns:
        enthalpy :math:`H` in :math:`[M^{1} L^{2} t^{-2}]`
    """
    return h * m

def enthalpy(**kwargs):
    r"""
    Calculate the enthalpy :math:`H` in :math:`[M^{1} L^{2} t^{-2}]` from the specific enthalpy :math:`h`

    .. math::
       H = h m

    Args:
        h: Specific enthalpy :math:`h` in :math:`[L^{2} t^{-2}]`
        m: mass :math:`m` in :math:`[M^{1}]`

    Returns:
        enthalpy :math:`H` in :math:`[M^{1} L^{2} t^{-2}]`
    """
    return function_switch(enthalpy_from_specific_enthalpy,
                           **kwargs)
