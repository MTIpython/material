from CoolProp.HumidAirProp import HAPropsSI as hapropsi
from numpy import vectorize

from MTIpython.reference.bibliography import citation
from MTIpython.units.SI import u

HAPropsSI = vectorize(hapropsi)

__all__ = ['MTICoolpropHA']


class MTICoolpropHA:
    source = r"""
        @article{doi:10.1021/ie4033999,
        author = {Bell, Ian H. and Wronski, Jorrit and Quoilin, Sylvain and Lemort, Vincent},
        title = {Pure and Pseudo-pure Fluid Thermophysical Property Evaluation and
                 the Open-Source Thermophysical Property Library CoolProp},
        journal = {Industrial \& Engineering Chemistry Research},
        volume = {53},
        number = {6},
        pages = {2498--2508},
        year = {2014},
        doi = {10.1021/ie4033999},
        URL = {http://pubs.acs.org/doi/abs/10.1021/ie4033999},
        eprint = {http://pubs.acs.org/doi/pdf/10.1021/ie4033999}
    }
    """

    @staticmethod
    @citation(source)
    def specific_volume(T, p, RH):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return HAPropsSI('Vha', 'P', p.to('Pa').m, 'T', T.to('K').m, 'RH', RH.m) * u.m ** 3 / u.kg

    @staticmethod
    @citation(source)
    def temperature(p, v, RH):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return HAPropsSI('T', 'P', p.to('Pa').m, 'RH', RH.m, 'Vha', v.to('m**3/kg')) * u.K

    @staticmethod
    @citation(source)
    def specific_heat_const_pressure(p, T, RH):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return HAPropsSI('Cha', 'P', p.to('Pa').m, 'RH', RH.m, 'T', T.to('K').m) * u.J / (
                u.kg * u.K)

    @staticmethod
    @citation(source)
    def dynamic_viscosity(p, T, RH):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return HAPropsSI('mu', 'P', p.to('Pa').m, 'RH', RH.m, 'T', T.to('K').m) * u.Pa * u.s

    @staticmethod
    @citation(source)
    def conductivity(p, T, RH):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return HAPropsSI('k', 'P', p.to('Pa').m, 'RH', RH.m, 'T', T.to('K').m) * u.W / (u.m * u.K)

    @staticmethod
    @citation(source)
    def water_content(p, T, RH):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return HAPropsSI('W', 'P', p.to('Pa').m, 'RH', RH.m, 'T', T.to('K').m) * 1000 * u.g / u.kg

    @staticmethod
    @citation(source)
    def specific_enthalpy(p, T, RH):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return HAPropsSI('Hha', 'P', p.to('Pa').m, 'RH', RH.m, 'T', T.to('K').m) * u.J / u.kg

    @staticmethod
    @citation(source)
    def specific_entropy(p, T, RH):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return HAPropsSI('Sha', 'P', p.to('Pa').m, 'RH', RH.m, 'T', T.to('K').m) * u.J / (
                    u.kg * u.K)

    @staticmethod
    @citation(source)
    def wetbulb_temperature(p, T, RH):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return HAPropsSI('Twb', 'P', p.to('Pa').m, 'RH', RH.m, 'T', T.to('K').m) * u.K

    @staticmethod
    @citation(source)
    def relative_humidity(p, T, W):
        r"""
        Wrapper for the Coolprop PropsSI function

        Args:
            material (str): Coolprop name
            T (:class:`~pint:.Quantity`): Temperature :math:`T` in :math:`[T]`
            p (:class:`~pint:.Quantity`): Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        """
        return HAPropsSI('RH', 'P', p.to('Pa').m, 'W', W.to('kg/kg').m, 'T',
                         T.to('K').m) * u.dimensionless
