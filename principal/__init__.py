r"""
.. module:: MTIpython.materials
    :platform: Unix, Windows
    :synopsis: packages for principal material calculations

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from MTIpython.material.principal import core, psychrometrics

__all__ = ['core', 'psychrometrics']
