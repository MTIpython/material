r"""
.. module:: MTIpython.material.materialfactory
    :platform: Unix, Windows
    :synopsis: A material factory.

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""

from dill import load
from pkg_resources import resource_filename, resource_listdir

from MTIpython.core.core import Singleton
from MTIpython.core.exception import LoadException
from MTIpython.core.logging import logged

__all__ = ['material_factory']


@logged
class MaterialFactory(metaclass=Singleton):
    list = {'all': []}
    _category = {}

    def __init__(self):
        r"""
        An interface class from which a user can obtain standard, predefined Materials whom inherit from
        :class:`.Matter`. The MaterialFactory is a Singleton. Common usage is through the object
        :obj:`.material_factory`.

            >>> from MTIpython.material.materialfactory import MaterialFactory, material_factory
            >>> m_fact = MaterialFactory()
            >>> m_fact is material_factory
            True
            >>> print(m_fact.list['all'])
            ['s235jr', 's275jr', 's355jr', 'seawater_01', 'seawater_02', 'seawater_03', 'seawater_04', 'seawater_05',
             'seawater_06', 'seawater_07', 'seawater_08', 'seawater_09', 'seawater_10', 'seawater_11', 'seawater_12',
              'water']
        """
        for category in resource_listdir('MTIpython', 'resources/materials'):
            if category != 'data':
                self.list[category] = [r.split('.')[0] for r in
                                       resource_listdir('MTIpython', 'resources/materials/' + category + '/')]
                self.list['all'].extend(self.list[category])
                for r in self.list[category]:
                    self._category[r] = category
        self.logger.info('Found the following materials: {}'.format(self.list))

    def __getitem__(self, item):
        if item not in self.list['all']:
            raise KeyError('Material {} not found in package resources!'.format(item))
        filename = resource_filename('MTIpython',
                                     'resources/materials/' + self._category[item] + '/' + item + '.mtimat')
        with open(filename, 'rb') as f:
            try:
                return load(f)
            except ModuleNotFoundError as e:
                raise LoadException(e.args,
                                    'Stored class incompatible with current class version')

    def load_custom(self, filename):
        r"""
        Load a custom stored material

        Args:
            filename (str): Loads a custom based material

        Returns:
            material
        """
        with open(filename, 'rb') as f:
            try:
                return load(f)
            except ModuleNotFoundError as e:
                raise LoadException(e.args,
                                    'Stored class incompatible with current class version')


material_factory = MaterialFactory()
r"""
A material factory, listing all standard available materials, and allows for loading of a standard stored material.

Example usage:

    >>> from MTIpython.material import material_factory
    >>> print(material_factory.list['all'])
    ['s235jr', 's275jr', 's355jr', 'seawater_01', 'seawater_02', 'seawater_03', 'seawater_04', 'seawater_05',
     'seawater_06', 'seawater_07', 'seawater_08', 'seawater_09', 'seawater_10', 'seawater_11', 'seawater_12',
      'water']
    >>> print(material_factory.list['metals'])
    ['s235jr', 's275jr', 's355jr']
    >>> water = material_factory['water']
    >>> water
    name: Water
    sn: Wa
    category: Category.FLUID
    T: 20.0 celsius
    rho: 9.98e+02 kg/m³
    p: 1.0 bar
    c_p: 4.18e+03 J/K/kg
    v: 0.001 m³/kg
    k: 0.598 W/K/m
    gamma: 9.79e+03 N/m³
    M: None
    m: None
    V: None
    nu: 1e-06 m²/s
    mu: 0.001 Pa·s
    Pr: None
"""
