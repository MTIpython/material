r"""MTIpython.material.solid
   :platform: Unix, Windows
   :synopsis: Solid materials

.. :moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>

"""
from MTIpython.core.binder import Binder, Valid
from MTIpython.material.base import Matter
from MTIpython.mtimath.types import inf
from MTIpython.units.SI import u

__all__ = ['Solid']


class Solid(Matter):
    def __init__(self, E=None, epsilon=None, rel_cost=None, **kwargs):
        r"""
        The superclass for all solids, This class describes the basic properties, which all solids have, such as an
        elastic modulus.

        Args:
            name (str): he common name for the material i.e. S235JR or Water
            rho (:class:`~pint:.Quantity` or :class:`.Binder`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
            T (:class:`~pint:.Quantity` or :class:`.Binder`): Temperature :math:`T` in :math:`[T]`
            category (:class:`~Category`): The Material category
            c_p (:class:`~pint:.Quantity` or :class:`.Binder`): Specific heat at constant pressure :math:`c_p` in
             :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`~pint:.Quantity` or :class:`.Binder`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`~pint:.Quantity` or :class:`.Binder`): Mass :math:`m` in :math:`[M^{1}]`
            V (:class:`~pint:.Quantity` or :class:`.Binder`): Volume :math:`V` in :math:`[L^{3}]`
            v (:class:`~pint:.Quantity` or :class:`.Binder`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
            k (:class:`~pint:.Quantity` or :class:`.Binder`): Thermal conductivity :math:`k` in
             :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`~pint:.Quantity` or :class:`.Binder`): Specific weight :math:`\gamma` in
             :math:`[M^{1} L^{-2} t^{-2}]`
            E (:class:`~pint:.Quantity` or :class:`.Binder`): Elastic modulus :math:`E` in :math:`[M^{1} L^{-1} t^{-2}]`
            epsilon (:class:`~pint:.Quantity` or :class:`.Binder`): Surface roughness :math:`\varepsilon` in
             :math:`[L^{1}]`
            rel_cost (:class:`~pint:.Quantity` or :class:`.Binder`): Relative material cost, compared with steel S235JR
             in :math:`\left[-\right]`, these values can be obtained from : Muhs, Dieter, and Wilhelm Matek.
             Roloff/Matek machine-onderdelen: tabellenboek. SDU Uitgevers, n.d.
            CAS: Chemical Abstracts Service number
            **kwargs: User set attributes

        Attributes:
            name (str): he common name for the material i.e. S235JR or Water
            sn (str): str: Short name for the material. When the :attr:`.name` consists of multiple  words, the short
             name is build from all first letters. When the name consist of a single word, the first two letters are
             used
            rho (:class:`.Valid`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`.
             rho is bound with (:attr:`.v`) or (:attr:`.gamma`) or (:attr:`.V` and :attr:`.m`) using :func:`.density`
             function
            T (:class:`.Valid`): Temperature :math:`T` in :math:`[T]`
            category (:class:`~Category`): The Material category
            c_p (:class:`.Valid`): Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`.Valid`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`.Valid`): Mass :math:`m` in :math:`[M^{1}]`
             m is bound with (:attr:`.rho` and :attr:`.V`) or (:attr:`.gamma` and :attr:`.v`) or (:attr:`.v` and
             :attr:`.V`) using :func:`.mass` function
            V (:class:`.Valid`): Volume :math:`V` in :math:`[L^{3}]`.
             V is bound with (:attr:`.rho` and :attr:`.m`) or (:attr:`.gamma` and :attr:`.m`) or (:attr:`.v` and
             :attr:`.m`) using :func:`.volume` function
            v (:class:`.Valid`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`.
             v is bound with (:attr:`.rho`) or (:attr:`.m` and :attr:`.V`) using :func:`.specific_volume` function
            k (:class:`.Valid`): Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`.Valid`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`.
             gamma is bound with (:attr:`.rho`) or (:attr:`.v`) or (:attr:`.m` and :attr:`.V`) using
             :func:`.specific_weight` function
            E (:class:`.Valid`): Elastic modulus :math:`E` in :math:`[M^{1} L^{-1} t^{-2}]`
            epsilon (:class:`.Valid`): Surface roughness :math:`\varepsilon` in :math:`[L^{1}]`
            rel_cost (:class:`.Valid`): Relative material cost, compared with steel S235JR
             in :math:`\left[-\right]`, these values can be obtained from : Muhs, Dieter, and Wilhelm Matek.
             Roloff/Matek machine-onderdelen: tabellenboek. SDU Uitgevers, n.d.
            CAS: Chemical Abstracts Service number

        Note:
           Each creation triggers an info log entry, specifying the material specifications
        """
        self.E = Binder(value=E, bounds=(-inf, inf), unit=u.MPa)
        self.epsilon = Binder(value=epsilon, bounds=(0., inf), unit=u.mm)
        self.rel_cost = Binder(value=rel_cost, bounds=(-inf, inf), unit=u.dimensionless)
        super(Solid, self).__init__(**kwargs)

    E = Valid()
    r""":class:`.Valid`: Elastic modulus :math:`E` in :math:`[M^{1} L^{-1} t^{-2}]`"""

    epsilon = Valid()
    r""":class:`.Valid`: Surface roughness :math:`\varepsilon` in :math:`[L^{1}]`"""

    rel_cost = Valid()
    r""":class:`.Valid`: Relative material cost, compared with steel S235JR in 
    :math:`\left[-\right]`, these values can be obtained from : Muhs, Dieter, and Wilhelm Matek. Roloff/Matek machine-
    onderdelen: tabellenboek. SDU Uitgevers, n.d."""

    _mat = ['name', 'sn', 'CAS', 'category', 'T', 'rho', 'c_p', 'v', 'k', 'gamma', 'M', 'm', 'V', 'E', 'epsilon',
            'rel_cost']
    """list(str): the variables which describe the state of the material. Override this list for each child """
