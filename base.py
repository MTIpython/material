r"""
.. module:: MTIpython.material.base
    :platform: Unix, Windows
    :synopsis: Base class for all materials.

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""

from enum import Enum

from MTIpython.core.binder import Binder, Function, Valid
from MTIpython.core.logging import logged
from MTIpython.core.mtiobj import MTIobj
from MTIpython.material.principal.core import density, mass, specific_volume, specific_weight, volume
from MTIpython.mtimath.types import inf
from MTIpython.units.SI import *

__all__ = ['Category', 'Matter']


class Category(Enum):
    """ Categories of different materials """

    UNDEFINED = 0
    METAL = 1
    PLASTIC = 2
    FLUID = 3
    GAS = 4
    BINGHAM = 5
    GRANULAR = 6
    POWDER = 7


@logged
class Matter(MTIobj):

    def __init__(self, name='not specified', rho=None, T=None, category=Category.UNDEFINED, c_p=None, M=None, m=None,
                 V=None, v=None, gamma=None, k=None, CAS=None, m_dot=None, V_dot=None, t=None, **kwargs):
        r"""
        The Matter superclass. All materials inherit from this class. This class describes the basic properties,
        which all mater have, such as a *density*, *specific weight*, and *temperature*.

        Args:
            name (str): he common name for the material i.e. S235JR or Water
            rho (:class:`~pint:.Quantity` or :class:`.Binder`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
            T (:class:`~pint:.Quantity` or :class:`.Binder`): Temperature :math:`T` in :math:`[T]`
            category (:class:`~Category`): The Material category
            c_p (:class:`~pint:.Quantity` or :class:`.Binder`): Specific heat at constant pressure :math:`c_p` in
             :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`~pint:.Quantity` or :class:`.Binder`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`~pint:.Quantity` or :class:`.Binder`): Mass :math:`m` in :math:`[M^{1}]`
            V (:class:`~pint:.Quantity` or :class:`.Binder`): Volume :math:`V` in :math:`[L^{3}]`
            v (:class:`~pint:.Quantity` or :class:`.Binder`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
            k (:class:`~pint:.Quantity` or :class:`.Binder`): Thermal conductivity :math:`k` in
             :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`~pint:.Quantity` or :class:`.Binder`): Specific weight :math:`\gamma` in
             :math:`[M^{1} L^{-2} t^{-2}]`
            CAS: Chemical Abstracts Service number
            **kwargs: User set attributes

        Attributes:
            name (str): he common name for the material i.e. S235JR or Water
            sn (str): str: Short name for the material. When the :attr:`.name` consists of multiple  words, the short
             name is build from all first letters. When the name consist of a single word, the first two letters are
             used
            rho (:class:`.Valid`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`.
             rho is bound with (:attr:`.v`) or (:attr:`.gamma`) or (:attr:`.V` and :attr:`.m`) using :func:`.density`
             function
            T (:class:`.Valid`): Temperature :math:`T` in :math:`[T]`
            category (:class:`~Category`): The Material category
            c_p (:class:`.Valid`): Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            M (:class:`.Valid`): Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m (:class:`.Valid`): Mass :math:`m` in :math:`[M^{1}]`
             m is bound with (:attr:`.rho` and :attr:`.V`) or (:attr:`.gamma` and :attr:`.v`) or (:attr:`.v` and
             :attr:`.V`) using :func:`.mass` function
            V (:class:`.Valid`): Volume :math:`V` in :math:`[L^{3}]`.
             V is bound with (:attr:`.rho` and :attr:`.m`) or (:attr:`.gamma` and :attr:`.m`) or (:attr:`.v` and
             :attr:`.m`) using :func:`.volume` function
            v (:class:`.Valid`): Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`.
             v is bound with (:attr:`.rho`) or (:attr:`.m` and :attr:`.V`) using :func:`.specific_volume` function
            k (:class:`.Valid`): Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma (:class:`.Valid`): Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`.
             gamma is bound with (:attr:`.rho`) or (:attr:`.v`) or (:attr:`.m` and :attr:`.V`) using
             :func:`.specific_weight` function
            CAS: Chemical Abstracts Service number

        Note:
            Each creation triggers an info log entry, specifying the material specifications
        """
        self.name = name
        self.category = category
        self.rho = Binder(value=rho, bounds=(0., inf), unit=u.kg / u.m ** 3,
                          func1=Function(density, v='v'),
                          func2=Function(density, gamma='gamma'),
                          func3=Function(density, V='V', m='m'))
        self.T = Binder(value=T, bounds=(0., inf), unit=u.degC)
        self.t = Binder(value=t, bounds=(-inf, inf), unit=u.s)
        self.m_dot = Binder(value=m_dot, bounds=(-inf, inf), unit=u.kg / u.s)
        self.V_dot = Binder(value=V_dot, bounds=(-inf, inf), unit=u.m ** 3 / u.s)
        self.c_p = Binder(value=c_p, bounds=(-inf, inf), unit=u.J / (u.kg * u.K))
        self.M = Binder(value=M, bounds=(0., inf), unit=u.kg / u.mol)
        self.m = Binder(value=m, bounds=(0., inf), unit=u.kg,
                        func1=Function(mass, rho='rho', V='V'),
                        func2=Function(mass, gamma='gamma', V='V'),
                        func3=Function(mass, v='v', V='V'))
        self.V = Binder(value=V, bounds=(0., inf), unit=u.m ** 3,
                        func1=Function(volume, rho='rho', m='m'),
                        func2=Function(volume, gamma='gamma', m='m'),
                        func3=Function(volume, v='v', m='m'))
        self.v = Binder(value=v, bounds=(0., inf), unit=u.m ** 3 / u.kg,
                        func1=Function(specific_volume, rho='rho'),
                        func2=Function(specific_volume, m='m', V='V'))
        self.gamma = Binder(value=gamma, bounds=(0., inf), unit=u.N / u.m ** 3,
                            func1=Function(specific_weight, rho='rho'),
                            func2=Function(specific_weight, v='v'),
                            func3=Function(specific_weight, m='m', V='V'))
        self.k = Binder(value=k, bounds=(0., inf), unit=u.W / (u.m * u.K))
        self.CAS = CAS
        super(Matter, self).__init__(**kwargs)
        self.logger.info('Matter created with the following specs: \n{}'.format(repr(self)))

    _version = 4
    """int: version of the Matter class. Bump this value up for big changes in the class which aren't compatible with 
    earlier release. """

    _mat = ['name', 'sn', 'CAS', 'category', 'T', 'rho', 'c_p', 'v', 'gamma', 'M', 'm', 'V']
    """list(int): the variables which describe the state of the material. Override this list for each child """

    name = None
    r"""str: The common name for the material i.e. S235JR or Water """

    CAS = None
    r"""str: Chemical Abstracts Service number"""

    category = None
    r""":class:`~Category` The Material category"""

    rho = Valid()
    r""":class:`.Valid` Density :math:`\rho` in :math:`[M^{1} L^{-3}]`"""

    gamma = Valid()
    r""":class:`.Valid` Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`"""

    v = Valid()
    r""":class:`.Valid` Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`"""

    T = Valid()
    r""":class:`.Valid` Temperature :math:`T` in :math:`[T]`"""

    t = Valid()
    r""":class:`.Valid` \Delta time for flow calculations :math:`t` in :math:`[t^{1}]`"""

    c_p = Valid()
    r""":class:`.Valid` Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`"""

    M = Valid()
    r""":class:`.Valid` Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`"""

    m = Valid()
    r""":class:`.Valid` Mass :math:`m` in :math:`[M^{1}]`"""

    m_dot = Valid()
    r""":class:`.Valid` Mass flow :math:`\dot{m}` in :math:`[M^{1} t^{-1}]`"""

    V = Valid()
    r""":class:`.Valid` Volume :math:`V` in :math:`[L^{3}]`"""

    V_dot = Valid()
    r""":class:`.Valid` Volume :math:`\dot{V}` in :math:`[L^{3} t^{-1}]`"""

    k = Valid()
    r""":class:`.Valid` Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`"""

    def __repr__(self):
        return self.name if self.name is not None else '<Undefined material>'
        # try:
        #     return ''.join(['<{}: {:.3~P}>\n'.format(m, getattr(self, m)) if isquantity(
        #         getattr(self, m)) else '<{}: {}>\n'.format(m, getattr(self, m)) for m in self._mat])[:-1]
        # except:
        #     return "Unable to get repr for <class '{}'>".format(type(self))

    def to_dict(self):
        dict_repr = {}
        for m in self._mat:
            if getattr(self, m) is not None:
                dict_repr[m] = getattr(self, m)
        return dict_repr

    @property
    def sn(self):
        r"""
        str: Short name for the material. When the :attr:`~name` consists of multiple words, the short name is
        build from all first letters. When the name consist of a single word, the first two letters are used """
        words = self.name.split(' ')
        if len(words) > 1:
            return ''.join([w[0] for w in words])
        return self.name[:2]
