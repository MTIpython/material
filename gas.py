r"""
.. module:: MTIpython.material.gas
    :platform: Unix, Windows
    :synopsis: Gas-like materials.

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from MTIpython.core.binder import Binder, Function, Valid
from MTIpython.fluid.principal.core import Re_newton
from MTIpython.material.base import Category, Matter
from MTIpython.material.principal.core import dynamic_viscosity, kinematic_viscosity
from MTIpython.mtimath.types import inf
from MTIpython.units.SI import u as ureg

__all__ = ['Gas', 'HumidAir']


class Gas(Matter):
    def __init__(self, p=None, mu=None, nu=None, R=None, kappa=None, c_v=None, Pr=None, h=None,
                 H=None, u=None, s=None, Z=None, **kwargs):
        r"""
        Gas is one of the four fundamental states of matter (the others being solid, liquid,
        and plasma). A pure gas may be made up of individual atoms (e.g. a noble gas like neon),
        elemental molecules made from one type of atom (e.g. oxygen), or compound molecules made
        from a variety of atoms (e.g. carbon dioxide). A gas mixture would contain a variety of
        pure gases much like the air. What distinguishes a gas from liquids and solids is the
        vast separation of the individual gas particles. This separation usually makes a
        colorless gas invisible to the human observer. The interaction of gas particles in the
        presence of electric and gravitational fields are considered negligible as indicated by
        the constant velocity vectors in the image. One type of commonly known gas is steam.
        source: `Wikipedia <https://en.wikipedia.org/wiki/Gas>`_

        Args:
            name (str): he common name for the material i.e. S235JR or Water
            rho: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
            T: Temperature :math:`T` in :math:`[T]`
            c_p: Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            M: Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m: Mass :math:`m` in :math:`[M^{1}]`
            V: Volume :math:`V` in :math:`[L^{3}]`
            v: Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
            k: Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma: Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`
            p: Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
            mu: Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
            nu: Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`
            R: Specific gas constant :math:`R` in :math:`[L^{2} T^{-1} t^{-2}]`
            kappa: Heat capacity ratio:math:`\kappa` in :math:`[-]`
            c_v: Specific heat at constant volume :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            Pr: Prandtl number :math:`Pr` in :math:`[-]`
            h: Specific enthalpy :math:`h` in :math:`[L^{2} t^{-2}]`
            H: Enthalpy :math:`H` in :math:`[M^{1} L^{2} t^{-2}]`
            u: Specific internal energy :math:`u` in :math:`[L^{2} t^{-2}]`
            s: Specific entropy :math:`s` in :math:`[L^{2} t^{-2} T^{-1}]`
            Z: Compressibility factor :math":`Z` in :math:`[-]`
            CAS: Chemical Abstracts Service number
            **kwargs: User set attributes

        Attributes:
            name: he common name for the material i.e. S235JR or Water
            sn: Short name for the material. When the :attr:`.name` consists of multiple  words, the
             short name is build from all first letters. When the name consist of a single word, the
             first two letters are used
            rho: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`. rho is bound with (:attr:`.v`) or
             (:attr:`.gamma`) or (:attr:`.V` and :attr:`.m`) using :func:`.density` function
            T: Temperature :math:`T` in :math:`[T]`
            category: The Material category
            c_p: Specific heat at constant pressure :math:`c_p` in :math:`[L^{ 2} T^{-1} t^{-2}]`
            M: Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m: Mass :math:`m` in :math:`[M^{1}]` m is bound with (:attr:`.rho` and :attr:`.V`) or
             (:attr:`.gamma` and :attr:`.v`) or (:attr:`.v` and :attr:`.V`) using :func:`.mass`
             function
            V: Volume :math:`V` in :math:`[L^{3}]`. V is bound with (:attr:`.rho` and :attr:`.m`) or
             (:attr:`.gamma` and :attr:`.m`) or (:attr:`.v` and :attr:`.m`) using :func:`.volume`
             function
            v: Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`. v is bound with (:attr:`.rho`)
             or (:attr:`.m` and :attr:`.V`) using :func:`.specific_volume` function
            k: Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{ -3}]`
            gamma: Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{ -2}]`. gamma is bound
             with (:attr:`.rho`) or (:attr:`.v`) or (:attr:`.m` and :attr:`.V`) using
             :func:`.specific_weight` function
            p: Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
            mu: Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`. mu is bound with
             (:attr:`.rho`, :attr:`.nu`) using :func:`.dynamic_viscosity`
            nu: Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`. nu is bound with
             (:attr:`.rho`, :attr:`.mu`) using :func:`.kinematic_viscosity`
            Pr: Prandtl number :math:`Pr` in :math:`[-]`
            R: Specific gas constant :math:`R` in :math:`[L^{2} T^{-1} t^{-2}]`
            kappa: Heat capacity ratio :math:`\kappa` in :math:`[-]`
            c_v: Specific heat at constant volume :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            h: Specific enthalpy :math:`h` in :math:`[L^{2} t^{-2}]`
            H: Enthalpy :math:`H` in :math:`[M^{1} L^{2} t^{-2}]`
            u: Specific internal energy :math:`u` in :math:`[L^{2} t^{-2}]`
            s: Specific entropy :math:`s` in :math:`[L^{2} t^{-2} T^{-1}]`
            Z: Compressibility factor :math":`Z` in :math:`[-]`
            CAS: Chemical Abstracts Service number

        Note:
            Each creation triggers an info log entry, specifying the material specifications
        """

        self.p = Binder(value=p, bounds=(0., inf), unit=ureg.Pa)
        self.nu = Binder(value=nu, bounds=(inf, -inf), unit=ureg.m ** 2 / ureg.s,
                         func1=Function(kinematic_viscosity, rho='rho', mu='mu'))
        self.mu = Binder(value=mu, bounds=(inf, -inf), unit=ureg.Pa * ureg.s,
                         func1=Function(dynamic_viscosity, rho='rho', nu='nu'))
        self.R = Binder(value=R, bounds=(-inf, inf), unit=ureg.J / (ureg.kg * ureg.K))
        self.kappa = Binder(value=kappa, bounds=(-inf, inf), unit=ureg.dimensionless)
        self.c_v = Binder(value=c_v, bounds=(-inf, inf), unit=ureg.J / (ureg.kg * ureg.K))
        self.Pr = Binder(value=Pr, bounds=(-inf, inf), unit=ureg.dimensionless)
        self.h = Binder(value=h, bounds=(-inf, inf), unit=ureg.J / ureg.kg)
        self.H = Binder(value=H, bounds=(-inf, inf), unit=ureg.J)
        self.u = Binder(value=u, bounds=(-inf, inf), unit=ureg.J / ureg.kg)
        self.s = Binder(value=s, bounds=(-inf, inf), unit=ureg.J / (ureg.kg * ureg.K))
        self.Z = Binder(value=Z, bounds=(-inf, inf), unit=ureg.dimensionless)
        super(Gas, self).__init__(category=kwargs.pop('category', Category.GAS), **kwargs)

    _version = 6
    """int: version of the Gas class. Bump this value up for big changes in the class which aren't
    compatible with earlier release. """

    p = Valid()
    r""":class:`.Valid`: Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`"""

    nu = Valid()
    r""":class:`.Valid`: Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`"""

    mu = Valid()
    r""":class:`.Valid`: Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`"""

    c_v = Valid()
    r""":class:`.Valid`: Specific heat at constant volume :math:`c_p` in :math:`[L^{2} T^{-1} 
    t^{-2}]`"""

    R = Valid()
    r""":class:`.Valid`: Specific gas constant :math:`R` :math:`[L^{2} T^{-1} t^{-2}]`"""

    kappa = Valid()
    r""":class:`.Valid`: Heat capacity ratio :math:`\kappa` in :math:`[-]`"""

    Pr = Valid()
    r""":class:`.Valid`: Prandtl number :math:`Pr` in :math:`[-]`"""

    h = Valid()
    r""":class:`.Valid`: Specific enthalpy :math:`h` in :math:`[L^{2} t^{-2}]`"""

    H = Valid()
    r""":class:`.Valid`: Enthalpy :math:`H` in :math:`[M^{1} L^{2} t^{-2}]`"""

    u = Valid()
    r""":class:`.Valid`: Specific internal energy :math:`u` in :math:`[L^{2} t^{-2}]`"""

    s = Valid()
    r""":class:`.Valid`: Specific entropy :math:`s` in :math:`[L^{2} t^{-2} T^{-1}]`"""

    Z = Valid()
    r""":class:`.valid`: Compressibility factor :math:`Z` in :math:`[-]`"""

    _mat = ['name', 'sn', 'CAS', 'category', 'T', 'rho', 'p', 'c_p', 'c_v', 'R', 'k', 'kappa', 'v', 'gamma', 'M', 'm',
            'V', 'nu', 'mu', 'Pr', 'h', 'H', 'u', 's', 'Z']

    _Re = staticmethod(Re_newton)
    r""" Calculate Reynolds with Re_newton"""


class HumidAir(Gas):
    def __init__(self, RH=None, W=None, T_wb=None, **kwargs):
        r"""
        Gas is one of the four fundamental states of matter (the others being solid, liquid,
        and plasma). A pure gas may be made up of individual atoms (e.g. a noble gas like neon),
        elemental molecules made from one type of atom (e.g. oxygen), or compound molecules made
        from a variety of atoms (e.g. carbon dioxide). A gas mixture would contain a variety of
        pure gases much like the air. What distinguishes a gas from liquids and solids is the
        vast separation of the individual gas particles. This separation usually makes a
        colorless gas invisible to the human observer. The interaction of gas particles in the
        presence of electric and gravitational fields are considered negligible as indicated by
        the constant velocity vectors in the image. One type of commonly known gas is steam.
        source: `Wikipedia <https://en.wikipedia.org/wiki/Gas>`_

        Args:
            name (str): he common name for the material i.e. S235JR or Water
            rho: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
            T: Temperature :math:`T` in :math:`[T]`
            c_p: Specific heat at constant pressure :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            M: Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m: Mass :math:`m` in :math:`[M^{1}]`
            V: Volume :math:`V` in :math:`[L^{3}]`
            v: Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`
            k: Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{-3}]`
            gamma: Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{-2}]`
            p: Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
            mu: Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
            nu: Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`
            R: Specific gas constant :math:`R` in :math:`[L^{2} T^{-1} t^{-2}]`
            kappa: Heat capacity ratio:math:`\kappa` in :math:`[-]`
            c_v: Specific heat at constant volume :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            Pr: Prandtl number :math:`Pr` in :math:`[-]`
            h: Specific enthalpy :math:`h` in :math:`[L^{2} t^{-2}]`
            H: Enthalpy :math:`H` in :math:`[M^{1} L^{2} t^{-2}]`
            u: Specific internal energy :math:`u` in :math:`[L^{2} t^{-2}]`
            s: Specific entropy :math:`s` in :math:`[L^{2} t^{-2} T^{-1}]`
            CAS: Chemical Abstracts Service number
            RH: Relative Humidity :math:`RH` in :math:`[-]`
            W: Water content ratio :math:`W` in :math:`[M^{1} M^{-1}]`
            T_wb: Wet bulb temperature :math:`T_{wb}` :math:`[K^{1}]`
            Z: Compressibility factor :math":`Z` in :math:`[-]`
            **kwargs: User set attributes

        Attributes:
            name: he common name for the material i.e. S235JR or Water
            sn: Short name for the material. When the :attr:`.name` consists of multiple  words, the
             short name is build from all first letters. When the name consist of a single word, the
             first two letters are used
            rho: Density :math:`\rho` in :math:`[M^{1} L^{-3}]`. rho is bound with (:attr:`.v`) or
             (:attr:`.gamma`) or (:attr:`.V` and :attr:`.m`) using :func:`.density` function
            T: Temperature :math:`T` in :math:`[T]`
            category: The Material category
            c_p: Specific heat at constant pressure :math:`c_p` in :math:`[L^{ 2} T^{-1} t^{-2}]`
            M: Molar mass :math:`M` in :math:`[M^{1} n^{-1}]`
            m: Mass :math:`m` in :math:`[M^{1}]` m is bound with (:attr:`.rho` and :attr:`.V`) or
             (:attr:`.gamma` and :attr:`.v`) or (:attr:`.v` and :attr:`.V`) using :func:`.mass`
             function
            V: Volume :math:`V` in :math:`[L^{3}]`. V is bound with (:attr:`.rho` and :attr:`.m`) or
             (:attr:`.gamma` and :attr:`.m`) or (:attr:`.v` and :attr:`.m`) using :func:`.volume`
             function
            v: Specific volume :math:`v` in :math:`[L^{3} M^{-1}]`. v is bound with (:attr:`.rho`)
             or (:attr:`.m` and :attr:`.V`) using :func:`.specific_volume` function
            k: Thermal conductivity :math:`k` in :math:`[L^{1} M^{1} T^{-1} t^{ -3}]`
            gamma: Specific weight :math:`\gamma` in :math:`[M^{1} L^{-2} t^{ -2}]`. gamma is bound
             with (:attr:`.rho`) or (:attr:`.v`) or (:attr:`.m` and :attr:`.V`) using
             :func:`.specific_weight` function
            p: Pressure :math:`p` in :math:`[M^{1} L^{-1} t^{-2}]`
            mu: Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`. mu is bound with
             (:attr:`.rho`, :attr:`.nu`) using :func:`.dynamic_viscosity`
            nu: Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`. nu is bound with
             (:attr:`.rho`, :attr:`.mu`) using :func:`.kinematic_viscosity`
            Pr: Prandtl number :math:`Pr` in :math:`[-]`
            R: Specific gas constant :math:`R` in :math:`[L^{2} T^{-1} t^{-2}]`
            kappa: Heat capacity ratio :math:`\kappa` in :math:`[-]`
            c_v: Specific heat at constant volume :math:`c_p` in :math:`[L^{2} T^{-1} t^{-2}]`
            h: Specific enthalpy :math:`h` in :math:`[L^{2} t^{-2}]`
            H: Enthalpy :math:`H` in :math:`[M^{1} L^{2} t^{-2}]`
            u: Specific internal energy :math:`u` in :math:`[L^{2} t^{-2}]`
            s: Specific entropy :math:`s` in :math:`[L^{2} t^{-2} T^{-1}]`
            CAS: Chemical Abstracts Service number
            RH: Relative Humidity :math:`RH` in :math:`[-]`
            W: Water content ratio :math:`W` in :math:`[M^{1} M^{-1}]`
            T_wb: Wet bulb temperature :math:`T_{wb}` :math:`[K^{1}]`
            Z: Compressibility factor :math":`Z` in :math:`[-]`

        Note:
            Each creation triggers an info log entry, specifying the material specifications
        """
        self.RH = Binder(value=RH, bounds=(0., 1.), unit=ureg.dimensionless)
        self.W = Binder(value=W, bounds=(0., inf), unit=ureg.g / ureg.kg)
        self.T_wb = Binder(value=T_wb, bounds=(-inf, inf), unit=ureg.degC)
        super(HumidAir, self).__init__(**kwargs)

    _propsi_name = 'Air'

    RH = Valid()
    r"""
    Relative Humidity :math:`RH` in :math:`[-]`
    """

    W = Valid()
    r"""
    Water content ratio :math:`W` in :math:`[M^{1} M^{-1}]`
    """

    T_wb = Valid()
    r"""
    Wet bulb temperature :math:`T_{wb}` :math:`[K^{1}]`
    """

    _mat = ['name', 'sn', 'CAS', 'category', 'T', 'rho', 'p', 'c_p', 'c_v', 'R', 'k', 'kappa', 'v', 'gamma', 'M', 'm',
            'V', 'nu', 'mu', 'Pr', 'h', 'H', 'u', 's', 'RH', 'W', 'T_wb', 'Z']
